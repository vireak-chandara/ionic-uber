angular.module('starter.controllers', [])

.controller('MapCtrl', function($scope, $http, $ionicLoading, $ionicPopup) {
  var directionsDisplay = new google.maps.DirectionsRenderer();
  var tarif = 20;
  var duree = "Inconnue";
  var distance = "Inconnue";
  var login = "";
  var password = "";
  $scope.mapCreated = function(map) {
    $scope.recherche = "";
    $scope.map = map;
    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      var image = '/img/park-132.png'
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      new google.maps.Marker({
          position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
          map: $scope.map,
          title: 'Moi'
      });
      new google.maps.Marker({
          position: new google.maps.LatLng(48.83, 2.26),
          map: $scope.map,
          title: 'Chauffeur',
          icon: image
      });
      new google.maps.Marker({
          position: new google.maps.LatLng(48.84, 2.28),
          map: $scope.map,
          title: 'Chauffeur',
          icon: image
      });
    }, function (error) {
      alert('Impossible de trouver votre position:' + error.message);
    });
  };

  $scope.addPoint = function(){
    
    directionsDisplay.suppressMarkers = true;
    if (!$scope.map) {
      return;
    }
    var destination = $scope.filter.destination;
    $scope.loading = $ionicLoading.show({
      content: 'Recherche d\'itinéraire',
      showBackdrop: false
    });


    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      var request = {
            origin      : new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
            destination : destination,
            travelMode  : google.maps.DirectionsTravelMode.DRIVING // Type de transport
      }

      var directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
      
      directionsDisplay.setMap($scope.map);
      directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
          if(status == google.maps.DirectionsStatus.OK){
              directionsDisplay.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
          }
      });
      $scope.loading.hide();
            
      //Calcul du tarif

      
      
      
      $http({ url: 'http://localhost:3002/tarif', 
                          method:"POST",
                          headers: {'Content-Type' : 'application/json', 'Access-Control-Allow-Origin' : 'http://localhost:3002'},
                          data:{'origin': origin, 'destination': destination}})
          .then(function successCallback(response) {
                console.log("succes" + response);
                tarif = response;
            }, function errorCallback(response) {
                console.log("erreur" + response);
      });
      console.log(tarif);

      $ionicPopup.show({
              title: 'Voulez-vous réserver ?',
              subTitle: 'Tarif: ' + tarif + "<br/>Durée: " + duree + "<br/>Distance: " + distance,
              scope: $scope,
              buttons: [
                { text: 'Annuler', onTap: function(e) { return true; } },
                {
                  text: '<b>Oui</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    return $scope.reservate() || true;
                  }
                },
              ]
              }).then(function(res) {
                console.log('Tapped!', res);
              }, function(err) {
                console.log('Err:', err);
              }, function(msg) {
                console.log('message:', msg);
              });
      
    });
  };

  $scope.centerOnMe = function () {
    console.log("Recentrage");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Obtention de votre position',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      new google.maps.Marker({
          position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
          map: $scope.map,
          title: 'Moi'
      });
      $scope.loading.hide();
    }, function (error) {
      alert('Impossible de trouver votre position: ' + error.message);
    });
  };

  $scope.reservate = function () {
    console.log("Réservation");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Réservation en cours',
      showBackdrop: false
    });

   //   API CALL RESERVATION

    $scope.loading.hide();
  };

  $scope.connexionClient = function() {
      /*
      $http({ url: 'http://localhost:3002/auth/clients', method:"POST",params:['email': email, 'password': password]});
    */
  }

  $scope.close = function() {
    $ionicLoading.hide();
  }

  $scope.openconnect = function() {
    $ionicPopup.prompt({
              title: 'Email',
              scope: $scope,
              buttons: [
                { text: 'Annuler', onTap: function(e) { return true; } },
                {
                  text: '<b>Suivant</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    return $scope.connexionClient() || true;
                  }
                },
              ]
              }).then(function(res) {
                console.log('Tapped!', res);
                login = res;
                $scope.openpassword();
              }, function(err) {
                console.log('Err:', err);
              }, function(msg) {
                console.log('message:', msg);
              });

  }

  $scope.openpassword = function() {
    $ionicPopup.prompt({
              title: 'Mot de passe',
              type: "password",
              scope: $scope,
              buttons: [
                { text: 'Annuler', onTap: function(e) { return true; } },
                {
                  text: '<b>Connexion</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    return $scope.connexionClient() || true;
                  }
                },
              ]
              }).then(function(res) {
                console.log('Tapped!', res);
                login = res;
              }, function(err) {
                console.log('Err:', err);
              }, function(msg) {
                console.log('message:', msg);
              });

  }
});
